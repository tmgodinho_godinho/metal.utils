package metal.utils.fileiterator.bench;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Iterator;

import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.TrueFileFilter;

import metal.utils.fileiterator.FileIterator;

public class Benchmark {

	public static void main(String[] args) throws FileNotFoundException {

		Options opt = new Options();
		Option pathOption = new Option("rootPath", true, "Root Path used in the benchmark.");
		pathOption.setRequired(true);
		Option modeOpt = new Option("mode", true, "Selects the Iterator version to use: [default; guava; apache]");
		
		opt.addOption(pathOption);
		opt.addOption(modeOpt);
		
		String rootFolder = null;
		String mode = null;
		try {
			CommandLine lineParser = new BasicParser().parse(opt, args);
			if(!lineParser.hasOption("rootPath"))
				throw new ParseException("Please specify a root path");
			rootFolder = lineParser.getOptionValue("rootPath");

			mode = lineParser.getOptionValue("mode", "default");
			System.out.println("Using Mode: "+mode);
			
		} catch (ParseException e) {
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp( e.getMessage(), opt );
			System.exit(-1);
		}
				
		File f = new File(rootFolder);
		long startTime = System.currentTimeMillis();
		
		Iterator<File> it = null;		
		if(mode.equalsIgnoreCase("default")){
			it = new FileIterator(f);
		}else if(mode.equalsIgnoreCase("apache")){
			it = FileUtils.iterateFiles(f, TrueFileFilter.INSTANCE, TrueFileFilter.INSTANCE);
		}else{
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp( "Invalid mode: Select one of [default; guava; apache]", opt );
			System.exit(-1);
		}
		
		int i = 0;

		while(it.hasNext()){
			@SuppressWarnings("unused")
			File ff = it.next();
			i++;
		}		
		System.out.println("Number Of Files: "+i);
		System.out.println("Elapse Time: "+(System.currentTimeMillis() - startTime)/1000);
	}
	

}
