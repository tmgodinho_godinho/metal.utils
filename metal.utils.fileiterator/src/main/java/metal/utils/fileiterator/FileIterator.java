package metal.utils.fileiterator;

import java.io.File;
import java.io.FilenameFilter;
import java.util.Collections;
import java.util.Iterator;

import org.apache.commons.io.filefilter.DirectoryFileFilter;
import org.apache.commons.io.filefilter.FileFileFilter;
import org.apache.commons.io.filefilter.FileFilterUtils;
import org.apache.commons.io.filefilter.IOFileFilter;

/**
 * Implementation of a File Iterator for big data scenarios.
 * 
 * This iterator behaves like an ordinary iterator, however, its modus operandi was change to adjust heavy duty scenarios.
 * It iterates files and folders recursively. Only filenames are kept in memory so the total amount of descriptors in memory is minimized. *
 * 
 * @author Tiago Marques Godinho, metaltmg@gmail.com
 *
 */
public class FileIterator implements Iterator<File> {

	private IOFileFilter fileFilter = null;
	private File root;
	private String[] filenames;
	private Iterator<File> fileIt;
	
	private int i;
	private boolean iteratingInFiles;
		
	/**
	 * Opens an iterator in the root folder, with no filtering strategy bound.
	 * 
	 * @param root The root folder.
	 */
	public FileIterator(File root) {
		this(root, null);
	}
	
	/**
	 * Opens an iterator in the root folder with the specified file filter.
	 * 
	 * @param root The root folder.
	 * @param fileFilter The filter used for selecting files.
	 */
	public FileIterator(File root, IOFileFilter fileFilter) {
		this.fileFilter = fileFilter;
		
		this.root = root;
		this.filenames = mList(true);
		this.i = 0;
		this.iteratingInFiles = true;
		this.fileIt = null;
	}
	
	private String[] mList(boolean files){
		
		FilenameFilter filter;
		if(files){
			if(this.fileFilter != null)
				filter = FileFilterUtils.and( FileFileFilter.FILE , this.fileFilter );
			else
				filter = FileFileFilter.FILE;
		}else{
			filter = DirectoryFileFilter.INSTANCE;
		}

		String[] ret = this.root.list(filter);
		if(ret == null)
			return new String[0];
		return ret;
	}
	
	private boolean switchIterationMode(){
		if(iteratingInFiles){
			
			this.iteratingInFiles = false;
			this.i = 0;
			this.fileIt = Collections.emptyIterator();
			this.filenames = mList(false);
			if(!hasNextFilename())
				return false;
			
			do{
				this.fileIt = new FileIterator(new File(root, nextFilename()), this.fileFilter);
			}while(!this.fileIt.hasNext() && hasNextFilename());
			
			return this.fileIt.hasNext();
		}

		return false;
	}
		
	private boolean hasNextFilename(){
		return i < filenames.length;
	}
	
	private String nextFilename(){
		if(!hasNextFilename())
			return null;
		
		String f = this.filenames[i];
		filenames[i] = null;
		i++;
		return f;
	}
	
	public boolean hasNext() {
		if(iteratingInFiles){
			if(hasNextFilename())
				return true;
			
			return switchIterationMode();
		}else{
			if(fileIt.hasNext())
				return true;
			
			//Reload another Iterator;
			if(!hasNextFilename())
				return false;
			
			do{
				this.fileIt = new FileIterator(new File(root, nextFilename()), this.fileFilter);
			}while(!this.fileIt.hasNext() && hasNextFilename());
			
			return this.fileIt.hasNext();
		}		
	}

	public File next() {
		// TODO Auto-generated method stub
		if(!hasNext())
			return null;
		
		if(iteratingInFiles){
			//System.err.println("Root: "+root.toString() + " Mode: "+iteratingInFiles+" I: "+i +"/"+this.filenames.length);
			return new File(root, nextFilename());
		}else{
			//System.err.println("Root: "+root.toString() + " Mode: "+iteratingInFiles+" I: "+i +"/"+this.filenames.length);
			return this.fileIt.next();
		}
	}

	public void remove() {
		throw new UnsupportedOperationException();
	}

}
